---
layout: handbook-page-toc
title: "Digital Marketing Programs"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Digital Marketing Programs (DMP)

The Digital Marketing team generates new prospects through paid channels by building & managing digital marketing campaigns, testing incremental changes for conversion rate improvement, and tracking campaign performance.

### Team
 
- [Matt Nguyen](https://gitlab.com/mnguyen4) - Manager, Digital Marketing
- [Leslie Stinson](https://gitlab.com/lstinson) - Digital Marketing Manager
- [Zac Badgley](https://gitlab.com/zbadgley) - Digital Marketing Manager

[Click here](/job-families/marketing/digital-marketing-programs-manager/) to learn about the Digital Marketing Programs role.

**To learn about our Digital Marketing strategy and processes at length, head over to our [Digital Marketing Management page.](/handbook/marketing/demand-generation/digital-marketing/digital-marketing-management/)**

## DMP responsibilities
Digital Marketing is responsible for planning, launching, and optimizing paid ad campaigns, with a focus on top of funnel demand generation to fill pipeline.  

## Reporting

Information about reporting done by the Digital Marketing Programs team and across the Marketing functional group can be found in the [Reporting](/handbook/marketing/demand-generation/digital-marketing/digital-marketing-management/) section of the Digital Marketing Management page.

## Digital Marketing Tools

We use a variety of tools to support other teams within the company. Details about the tech stack, who has access and the system admins are found on the [Tech Stack Applications](/handbook/business-ops/tech-stack-applications/#tech-stack-applications) page of the Business Operations handbook section.



